import os
import pandas as pd

top_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')

energy_data_file_path = os.path.join(top_path, 'scripts', 'postproc', 'energy_pproc.csv')
events_data_file_path = os.path.join(top_path, 'scripts', 'postproc', 'events_pproc.csv')
holidays_data_file_path = os.path.join(top_path, 'resources', 'holidays_2017.csv')

# Read data from CSV files
energy_data = pd.read_csv(energy_data_file_path, parse_dates=['timestamp'])
events_data = pd.read_csv(events_data_file_path, parse_dates=['event_start', 'event_end'])
holidays_data = pd.read_csv(holidays_data_file_path, parse_dates=['timestamp'])

energy_data['timestamp'] = energy_data['timestamp'] - pd.DateOffset(years=1)
events_data['event_start'] = events_data['event_start'] - pd.DateOffset(years=1)
events_data['event_end'] = events_data['event_end'] - pd.DateOffset(years=1)

energy_data.to_csv(os.path.join(top_path, 'scripts', 'postproc', 'energy_pproc.csv'), index=False)
events_data.to_csv(os.path.join(top_path, 'scripts', 'postproc', 'events_pproc.csv'), index=False)
holidays_data.to_csv(os.path.join(top_path, 'scripts', 'postproc', 'holidays_pproc.csv'), index=False)
