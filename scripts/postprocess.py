import os
import pandas as pd

top_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')

energy_data_file_path = os.path.join(top_path, 'resources', 'energy.csv')
events_data_file_path = os.path.join(top_path, 'resources', 'events.csv')
holidays_data_file_path = os.path.join(top_path, 'resources', 'holidays.csv')

energy_data = pd.read_csv(energy_data_file_path, parse_dates=['timestamp'])
events_data = pd.read_csv(events_data_file_path)
holidays_data = pd.read_csv(holidays_data_file_path, parse_dates=['timestamp'])

### ------------------------------------------------------------------------------------------
### EVENTS DATA - convert timestamps to UTC

list_of_row_dfs = []

for index, row in events_data.iterrows():
    event_start = pd.Timestamp(row.event_start).tz_convert('UTC')
    event_end = pd.Timestamp(row.event_end).tz_convert('UTC')

    new_df_row = pd.DataFrame({
        'ISO': [row.ISO],
        'utility_id': [row.utility_id],
        'program_name': [row.program_name],
        'event_start': [event_start],
        'event_end': [event_end],
        'timezone': [row.timezone]
    })
    
    list_of_row_dfs.append(new_df_row)

events_data_modif = pd.concat(list_of_row_dfs, ignore_index=True)
events_data_modif.to_csv(os.path.join(top_path, 'scripts', 'postproc', 'events_pproc.csv'), index=False)

### ------------------------------------------------------------------------------------------
### HOLIDAYS DATA - nothing to do

holidays_data.to_csv(os.path.join(top_path, 'scripts', 'postproc', 'holidays_pproc.csv'), index=False)

### ------------------------------------------------------------------------------------------
### ENERGY DATA - fill NaNs, add columns

max_consecutive_samples_to_fill = 12
energy_data_groups = {}
for building_id, group_orig in energy_data.groupby('building_id'):
    group = group_orig.copy()

    if len(pd.unique(group.timezone)) > 1:
        raise ValueError(f'Timezone is not unique for building_id: {building_id}')

    timezone = group.timezone.iloc[0]

    # convert timestamps to timezone
    group.timestamp = group.timestamp.dt.tz_convert(timezone)

    # sort by timestamp
    group.sort_values('timestamp', inplace=True)

    # set timestamp column as index
    group.set_index('timestamp', inplace=True, drop=True)

    # create regular datetime index
    idx = pd.date_range(start=group.index.min(), end=group.index.max(), freq='5min')

    # reindex DataFrame with the new DateTimeIndex (adds new rows)
    group = group.reindex(idx)

    # fill missing values for timezone and building_id in the new rows
    group.timezone = group.timezone.fillna(timezone)
    group.building_id = group.building_id.fillna(building_id)

    # interpolate missing values for cumulative_energy_kwh
    group.cumulative_energy_kwh = group.cumulative_energy_kwh.interpolate(method='linear', 
                                                                          limit=max_consecutive_samples_to_fill)

    # add column for energy consumption in kWh
    group['energy_kwh'] = group['cumulative_energy_kwh'].diff().shift(-1)

    # add column for energy load in kW
    group['energy_kw'] = group['energy_kwh'] * 12

    # drop rows with NaNs (those that were not interpolated)
    group.dropna(inplace=True)

    energy_data_groups[building_id] = group


for building_id, group in energy_data_groups.items():
    # convert timestamps back to UTC
    group.index = group.index.tz_convert('UTC')

energy_data_modif = pd.concat((group for _, group in energy_data_groups.items()))
energy_data_modif.to_csv(os.path.join(top_path, 'scripts', 'postproc', 'energy_pproc.csv'), index=True, index_label='timestamp')