import os
import sqlite3
import pandas as pd

top_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')

# Create sqlite3 db
conn = sqlite3.connect(os.path.join(top_path, 'scripts', 'db', 'cbl.db'))

energy_data_file_path = os.path.join(top_path, 'scripts', 'postproc', 'energy_pproc.csv')
events_data_file_path = os.path.join(top_path, 'scripts', 'postproc', 'events_pproc.csv')
holidays_data_file_path = os.path.join(top_path, 'scripts', 'postproc', 'holidays_pproc.csv')

# Read data from CSV files
energy_data = pd.read_csv(energy_data_file_path, parse_dates=['timestamp'])
events_data = pd.read_csv(events_data_file_path, parse_dates=['event_start', 'event_end'])
holidays_data = pd.read_csv(holidays_data_file_path, parse_dates=['timestamp'])

# Write data to SQLite database
energy_data.to_sql('energy', conn, if_exists='replace', index=False)
events_data.to_sql('events', conn, if_exists='replace', index=False)
holidays_data.to_sql('holidays', conn, if_exists='replace', index=False)

conn.commit()
conn.close()