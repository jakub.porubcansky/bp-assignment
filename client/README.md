# Customer Baseline Load Python Client

## Requirements

- Python >= 3.11
- Poetry package manager

## Installing dependencies

1. Create new virtual environment: `poetry shell`
2. Install Python dependencies: `poetry install`
3. Leave virtual environment: `exit`

## Usage examples

Calculating Customer Baseline Load:

```python
import pandas as pd
import pytz
import cbl_client

baseline_calculator = cbl_client.BaselineCalculator()

config = cbl_client.BaselineConfig(
    building_id='Building A',
    event_start=pd.Timestamp('2018-02-26T12:00:00-05:00'), 
    event_end=pd.Timestamp('2018-02-26T18:00:00-05:00'), 
    granularity=pd.Timedelta(hours=1),
    timezone=pytz.timezone('America/New_York')
)

baseline = baseline_calculator.calculate(config)
```

Getting energy data:

```python
energy_data = cbl_client.get_energy_data(
    building_id='Building A',
    start=pd.Timestamp('2018-05-17T00:00:00-04:00'), 
    end=pd.Timestamp('2018-05-30T00:00:00-04:00'), 
    granularity=pd.Timedelta(minutes=15))
```

Getting events data:

```python
events = cbl_client.get_events(
    start=pd.Timestamp('2018-02-01T00:00:00-05:00'), 
    end=pd.Timestamp('2018-08-01T00:00:00-04:00'))
```

Getting holidays:

```python
holidays = cbl_client.get_holidays(
    start=pd.Timestamp('2018-02-01T00:00:00-05:00'), 
    end=pd.Timestamp('2018-08-01T00:00:00-04:00'))
```