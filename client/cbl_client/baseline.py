from typing import Optional
from dataclasses import dataclass
import requests
from pytz.tzinfo import DstTzInfo
import pandas as pd

from .config import get_url, SUPPORTED_GRANULARITIES
from .utils import (
    text_to_dataframe,
    to_granularity_string,
    raise_on_error,
    to_utc
)


@dataclass
class BaselineConfig():
    building_id: str
    event_start: pd.Timestamp
    event_end: pd.Timestamp
    timezone: DstTzInfo
    granularity: pd.Timedelta

    def __post_init__(self) -> None:
        if not isinstance(self.building_id, str):
            raise ValueError("Building ID must be a string")

        if not isinstance(self.event_start, pd.Timestamp):
            raise ValueError("Event start must be a pandas Timestamp")

        if not isinstance(self.event_end, pd.Timestamp):
            raise ValueError("Event end must be a pandas Timestamp")

        if not isinstance(self.timezone, DstTzInfo):
            raise ValueError("Timezone must be a pytz timezone")

        if not isinstance(self.granularity, pd.Timedelta):
            raise ValueError("Granularity must be a pandas Timedelta")

        if self.granularity not in SUPPORTED_GRANULARITIES:
            supported_granularities_text = ', '.join(str(g) for g in SUPPORTED_GRANULARITIES)
            raise ValueError(f"Supported granularities are: {supported_granularities_text}")

        if self.event_start.date() != self.event_end.date():
            raise ValueError("Baseline window can't span multiple days in the target timezone")

        if self.event_start >= self.event_end:
            raise ValueError("Event end must be after event start")

        self.event_start = to_utc(self.event_start)
        self.event_end = to_utc(self.event_end)


class BaselineCalculator:

    def calculate(self, config: BaselineConfig) -> pd.DataFrame:
        endpoint_url = self.__get_baseline_url()
        payload = self.__create_payload(config)

        response = requests.post(endpoint_url, json=payload)
        raise_on_error(response)

        return text_to_dataframe(response.text, timestamp_columns=['timestamp'])

    def __create_payload(self, config: BaselineConfig) -> dict:
        payload = {
            "building_id": config.building_id,
            "start": config.event_start.isoformat(),
            "end": config.event_end.isoformat(),
            "timezone": config.timezone.zone,
            "granularity": to_granularity_string(config.granularity)
        }

        return payload

    def __get_baseline_url(self) -> str:
        return f"{get_url()}/calculate-baseline"
