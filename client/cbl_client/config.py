import os
import pandas as pd

SUPPORTED_GRANULARITIES = [
    pd.Timedelta('1hour'),
    pd.Timedelta('15min'),
    pd.Timedelta('5min')
]


def get_url() -> str:
    default_url = 'http://localhost:8000'
    return os.getenv('CBL_SERVICE_URL', default_url)
