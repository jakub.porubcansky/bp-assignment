from typing import TypeVar, Generic, Optional
from dataclasses import dataclass
import requests
import pandas as pd

from .config import get_url
from .utils import (
    text_to_dataframe,
    to_granularity_string,
    raise_on_error,
    to_utc
)

T = TypeVar('T')


@dataclass
class TimeInterval(Generic[T]):
    start: T
    end: T

    def __post_init__(self) -> None:
        if self.start > self.end:
            raise ValueError('Start of interval must be equal to or less than end of interval')


def get_energy_data(building_id: pd.Timestamp, start: pd.Timestamp,
                    end: pd.Timestamp, granularity: pd.Timedelta) -> pd.DataFrame:
    interval = TimeInterval(to_utc(start), to_utc(end))

    query_params = {
        "building_id": building_id,
        "start": interval.start.isoformat(),
        "end": interval.end.isoformat(),
        "granularity": to_granularity_string(granularity)
    }

    response = requests.get(f"{get_url()}/energy", params=query_params)
    raise_on_error(response)

    return text_to_dataframe(response.text, timestamp_columns=['timestamp'])


def get_holidays(start: pd.Timestamp, end: pd.Timestamp) -> pd.DataFrame:
    interval = TimeInterval(start, end)

    query_params = {
        "start": interval.start.date().isoformat(),
        "end": interval.end.date().isoformat(),
    }

    response = requests.get(f"{get_url()}/holidays", params=query_params)
    raise_on_error(response)

    return text_to_dataframe(response.text, timestamp_columns=['timestamp'])


def get_events(start: pd.Timestamp, end: pd.Timestamp) -> pd.DataFrame:
    interval = TimeInterval(to_utc(start), to_utc(end))

    query_params = {
        "start": interval.start.isoformat(),
        "end": interval.end.isoformat(),
    }

    response = requests.get(f"{get_url()}/events", params=query_params)
    raise_on_error(response)

    return text_to_dataframe(response.text, timestamp_columns=['event_start', 'event_end'])
