import io
import requests
import pandas as pd

from .exceptions import CBLServiceError


def text_to_dataframe(text: str, timestamp_columns: list[str]) -> pd.DataFrame:
    return pd.read_csv(io.StringIO(text), sep=',', parse_dates=timestamp_columns)


def to_granularity_string(granularity: pd.Timedelta) -> str:
    components = granularity.components
    days = f'{components.days}day' if components.days != 0 else ''
    hours = f'{components.hours}hour' if components.hours != 0 else ''
    minutes = f'{components.minutes}min' if components.minutes != 0 else ''
    seconds = f'{components.seconds}sec' if components.seconds != 0 else ''
    return f'{days}{hours}{minutes}{seconds}'

def to_utc(input: pd.Timestamp) -> pd.Timestamp:
    if input.tzinfo is None:
        return input.tz_localize('UTC')
    else:
        return input.tz_convert('UTC')

def raise_on_error(response: requests.Response) -> None:
    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as err:
        response_status_code = response.status_code

        try:
            response_error_message = response.json()['detail']
        except:
            response_error_message = f'HTTP error: {response.text}'

        exception_message = f'{response_error_message} (HTTP {response_status_code})'
        raise CBLServiceError(exception_message) from err
    
