# Customer Baseline Load (CBL) calculation assignment

The repository is organized as follows:

- `resources` folder contains assignment input CSV files
- `scripts` folder contains python scripts for data preprocessing and jupyter notebooks for data and results visualization
- `cbl-service` folder contains baseline calculation API service
- `client` folder contains Python API client

## Quick start

Use docker to launch the API service. Navigate to the `cbl-service` folder and call

```
> docker-compose up
```

There is also an option to run it without docker, see cbl-service README file.

To install the Python client navigate to the client folder and call

```
> poetry shell
> poetry install
```

This creates a new poetry environment and installs the required dependencies. If you now run Python inside the Poetry environment you should be able to import the client

```python
>>> import cbl_client
```

You can find some usage examples in the client README file.

## Data preprocessing

The data from `resources` folder were postprocessed by `scripts/postprocess.py` script. The postprocessing consists of adding `energy_kwh` and `energy_kw` columns to energy data, imputing missing values (1 hour was chosen to be the max gap length to be imputed) and converting event data timestamps to UTC. The output CSV files were written to the `scripts/postproc` folder.

The `scripts/create_db.py` script takes the data from the postproc folder to create sqlite3 db file that is saved to `scripts/db` folder. This file was then manually copied to cbl-service which uses it for data access.

## Baseline calculation evaluation

The `examples.ipynb` notebook inside the `scripts` folder contains couple of examples of calculating Customer Baseline Load.

## Stranger things

The data exhibits daily and weekly patterns, as expected. 

![alt text](./img/image.png)

But after a closer look (check `scripts/visualization.ipynb` notebook) I noticed that those two days with the lower energy consumption repeating every week that I would expect to be saturdays and sundays are actually sundays and mondays. Some of the holidays also showed unexpected energy profiles. Notice the Thanksgiving day - the consumption profiles during that day seem to be comparable to other weekdays but notice the day after, that one looks like a holiday more. 

I am not sure this really is a data issue but just as an experiment I tried to change the 2018 year to 2017 for all the data (`scripts/postprocess_plan_b.py`) and use 2017 holidays (`resources/holidays_2017.csv`) to mitigate the issues. The cbl-service contains two sqlite3 db files `cbl.db` and `cbl_plan_b.db` for 2018 and 2017 data respectively and switching between them means just rewriting the `DB_FILE_NAME` variable in `cbl-service\api\endpoints.py` and restarting the service. 

Of course I kept the baseline calculation examples in `scripts/examples.ipynb` to be calculated on the 2018 data but I tried to run it also on the 2017 data and it seemed much more reasonable to me.