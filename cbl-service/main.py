from fastapi import FastAPI, HTTPException
from exceptions import ValidationException

from api.endpoints import router

app = FastAPI()


@app.exception_handler(ValidationException)
async def http_exception_handler(request, exc):
    raise HTTPException(status_code=400, detail=exc.args[0])

app.include_router(router)