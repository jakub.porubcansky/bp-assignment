from typing import TypeVar, Generic
from dataclasses import dataclass
from pytz.tzinfo import DstTzInfo
import pandas as pd
from exceptions import ValidationException

T = TypeVar('T')


@dataclass
class TimeInterval(Generic[T]):
    start: T
    end: T

    def __post_init__(self):
        if self.start > self.end:
            raise ValueError('Start of interval must be equal to or less than end of interval')


@dataclass
class CBLCalculationConfig():
    building_id: str
    event_window: TimeInterval[pd.Timestamp]
    timezone: DstTzInfo
    granularity: pd.Timedelta

    def __post_init__(self):
        if self.event_window.start.date() != self.event_window.end.date():
            raise ValidationException("Event window can't span multiple days")

    @property
    def is_weekend_baseline(self) -> bool:
        return self.event_window.start.dayofweek >= 5
