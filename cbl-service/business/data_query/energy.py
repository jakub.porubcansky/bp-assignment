from typing import Optional
import pandas as pd
from business.data_access import (
    DataAccessInterface,
    ENERGY_METER,
    ENERGY_TIMEZONE,
    ENERGY_TIMESTAMP
)
from business.models import TimeInterval


def get_energy(data_accessor: DataAccessInterface, building_id: str, datetime_interval: TimeInterval[pd.Timestamp],
               granularity: pd.Timedelta) -> pd.DataFrame:
    
    energy_data = data_accessor.get_energy_data(building_id=building_id, datetime_interval=datetime_interval)
    energy_data = _select_columns(energy_data)
    energy_data = _resample(energy_data, granularity)
    energy_data['energy_kw'] = _from_kwh_to_kw(energy_data[ENERGY_METER], granularity)
    return energy_data


def _select_columns(energy_data: pd.DataFrame) -> pd.DataFrame:
    return energy_data[[ENERGY_TIMESTAMP, ENERGY_TIMEZONE, ENERGY_METER]]


def _resample(energy_data: pd.DataFrame, granularity: pd.Timedelta) -> pd.DataFrame:
    aggregation_functions = _aggregation_fns()

    resampled = energy_data.resample(on=ENERGY_TIMESTAMP, rule=granularity)\
        .agg(aggregation_functions).reset_index(drop=False)
    
    resampled_without_missing = resampled.dropna(how='any')

    return resampled_without_missing


def _aggregation_fns() -> dict[str, callable]:
    return {
        ENERGY_TIMEZONE: lambda x: x.iloc[0] if not x.empty else None,
        ENERGY_METER: lambda x: sum(x) if not x.empty else None
    }


def _from_kwh_to_kw(kwh_series: pd.Series, granularity: pd.Timedelta) -> pd.Series:
    return kwh_series * (pd.Timedelta(hours=1) / granularity)