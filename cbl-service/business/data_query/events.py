import pandas as pd
from business.data_access import DataAccessInterface
from business.models import TimeInterval


def get_events(data_accessor: DataAccessInterface, datetime_interval: TimeInterval[pd.Timestamp]) -> pd.DataFrame:
    return data_accessor.get_events_data(datetime_interval)
