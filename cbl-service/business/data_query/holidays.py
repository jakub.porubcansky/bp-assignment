from datetime import date
import pandas as pd
from business.data_access import DataAccessInterface
from business.models import TimeInterval


def get_holidays(data_accessor: DataAccessInterface, date_interval: TimeInterval[date]) -> pd.DataFrame:
    return data_accessor.get_holidays_data(date_interval)
