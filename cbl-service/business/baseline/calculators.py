from typing import Protocol
from datetime import date, time
from pytz.tzinfo import DstTzInfo
import pandas as pd
from business.models import CBLCalculationConfig, TimeInterval
from business.data_access import (
    DataAccessInterface,
    ENERGY_TIMESTAMP,
    ENERGY_METER
)


class CBLBasisEstablisherInterface(Protocol):
    def establish(self, config: CBLCalculationConfig) -> list[date]:
        ...


class BaselineFromBasisCalculatorInterface(Protocol):
    def calculate(self, baseline_basis: list[date], config: CBLCalculationConfig) -> pd.DataFrame:
        ...


class CBLCalculator:
    def __init__(self, basis_establisher: CBLBasisEstablisherInterface,
                 baseline_calculator: BaselineFromBasisCalculatorInterface) -> None:
        self.basis_establisher = basis_establisher
        self.baseline_calculator = baseline_calculator

    def establish_cbl_basis(self, config: CBLCalculationConfig) -> list[date]:
        return self.basis_establisher.establish(config)

    def calculate_baseline(self, baseline_basis: list[date], config: CBLCalculationConfig) -> pd.DataFrame:
        return self.baseline_calculator.calculate(baseline_basis, config)


class BaselineFromBasisCalculator:
    def __init__(self, data_interface: DataAccessInterface) -> None:
        self.data_interface = data_interface

    def calculate(self, baseline_basis: list[date], config: CBLCalculationConfig) -> pd.DataFrame:
        energy_data = self._get_postprocessed_energy_data(baseline_basis, config)
        baseline = self._calculate(energy_data, config.granularity)
        baseline = self._add_date_and_timezone_information_to_baseline(baseline, config.event_window, config.timezone)
        baseline = self._convert_to_utc(baseline)
        return baseline

    def _get_postprocessed_energy_data(self, baseline_basis: list[date], config: CBLCalculationConfig) -> pd.DataFrame:
        energy_data = self._get_energy_data(baseline_basis, config)
        energy_data.sort_values(by=ENERGY_TIMESTAMP, ascending=True, inplace=True)
        energy_data = self._resample_energy_data(energy_data, config)
        energy_data = self._filter_basis_dates(energy_data, baseline_basis)
        energy_data = self._filter_event_window_timestamps(energy_data, config.event_window)
        return energy_data

    def _get_energy_data(self, baseline_basis: list[date], config: CBLCalculationConfig) -> pd.DataFrame:
        datetime_interval = self._construct_datetime_interval_to_get_energy_data(baseline_basis, config.timezone)
        time_interval = self._construct_time_interval_to_get_energy_data(config.event_window)

        energy_data = self.data_interface.get_energy_data(
            building_id=config.building_id,
            datetime_interval=datetime_interval,
        )

        energy_data[ENERGY_TIMESTAMP] = energy_data[ENERGY_TIMESTAMP].dt.tz_convert(config.timezone)

        energy_data = energy_data[(energy_data[ENERGY_TIMESTAMP].dt.time >= time_interval.start) &
                                  (energy_data[ENERGY_TIMESTAMP].dt.time < time_interval.end)]
                
        return energy_data[[ENERGY_TIMESTAMP, ENERGY_METER]]

    def _resample_energy_data(self, energy_data: pd.DataFrame, config: CBLCalculationConfig) -> pd.DataFrame:
        aggregation_fns = self._get_aggregation_fns()
        resampled = energy_data.resample(on=ENERGY_TIMESTAMP, rule=config.granularity).agg(aggregation_fns)\
            .reset_index(drop=False)
        
        resampled_without_missing = resampled.dropna(how='any')

        return resampled_without_missing
    
    def _get_aggregation_fns(self) -> dict[str, callable]:
        return {
            ENERGY_METER: lambda x: sum(x) if not x.empty else None
        }
    
    def _filter_basis_dates(self, energy_data: pd.DataFrame, baseline_basis: list[date]) -> pd.DataFrame:
        return energy_data[energy_data[ENERGY_TIMESTAMP].dt.date.isin(baseline_basis)]
    
    def _filter_event_window_timestamps(self, energy_data: pd.DataFrame, event_window: TimeInterval[pd.Timestamp]) -> pd.DataFrame:
        return energy_data[(energy_data.timestamp.dt.time >= event_window.start.time()) &
                           (energy_data.timestamp.dt.time < event_window.end.time())]

    def _construct_datetime_interval_to_get_energy_data(self, baseline_basis: list[date], timezone: DstTzInfo) -> TimeInterval[pd.Timestamp]:
        return TimeInterval(
            pd.Timestamp.combine(min(baseline_basis), time(0)).replace(tzinfo=timezone),
            pd.Timestamp.combine(max(baseline_basis), time(0)).replace(tzinfo=timezone) + pd.Timedelta(days=1)
        )

    def _construct_time_interval_to_get_energy_data(self, event_window: TimeInterval[pd.Timestamp]) -> TimeInterval[time]:
        return TimeInterval(
            event_window.start.time(),
            event_window.end.time()
        )

    def _calculate(self, energy_data: pd.DataFrame, granularity: pd.Timedelta) -> pd.DataFrame:
        result = energy_data.groupby(energy_data.timestamp.dt.time).mean()
        result['baseline_kw'] = result[ENERGY_METER] * (pd.Timedelta(hours=1) / granularity)
        result.drop(columns=[ENERGY_TIMESTAMP, ENERGY_METER], inplace=True)
        result.reset_index(drop=False, inplace=True)
        return result

    def _add_date_and_timezone_information_to_baseline(self, baseline: pd.DataFrame, event_window: TimeInterval[pd.Timestamp],
                                                       timezone: DstTzInfo) -> pd.DataFrame:
        event_date = event_window.start.date()
        modified_baseline = baseline.copy()
        modified_baseline[ENERGY_TIMESTAMP] = baseline[ENERGY_TIMESTAMP].apply(lambda x: pd.Timestamp.combine(event_date, x).replace(tzinfo=timezone))
        return modified_baseline

    def _convert_to_utc(self, baseline: pd.DataFrame) -> pd.DataFrame:
        baseline_new = baseline.copy()
        baseline_new[ENERGY_TIMESTAMP] = baseline_new[ENERGY_TIMESTAMP].dt.tz_convert('UTC')
        return baseline_new
