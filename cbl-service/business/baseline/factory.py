from business.baseline.basis.average_weekday import AverageWeekdayCBLBasisEstablisher
from business.baseline.basis.average_weekend import AverageWeekendCBLBasisEstablisher
from business.baseline.calculators import CBLCalculator, BaselineFromBasisCalculator
from business.baseline.calculation import CBLCalculatorInterface
from business.models import CBLCalculationConfig
from business.data_access import DataAccessInterface


def cbl_calculator_factory(config: CBLCalculationConfig, data_interface: DataAccessInterface) -> CBLCalculatorInterface:
    if config.is_weekend_baseline:
        return CBLCalculator(
            AverageWeekendCBLBasisEstablisher(data_interface),
            BaselineFromBasisCalculator(data_interface)
        )
    else:
        return CBLCalculator(
            AverageWeekdayCBLBasisEstablisher(data_interface),
            BaselineFromBasisCalculator(data_interface)
        )
