from datetime import date
from typing import Protocol
import pandas as pd
from business.models import CBLCalculationConfig


class CBLCalculatorInterface(Protocol):
    def establish_cbl_basis(self, config: CBLCalculationConfig) -> list[date]:
        ...

    def calculate_baseline(self, baseline_basis: list[date], config: CBLCalculationConfig) -> pd.DataFrame:
        ...


def calculate_baseline(cbl_calculator: CBLCalculatorInterface, config: CBLCalculationConfig) -> pd.DataFrame:
    cbl_basis = cbl_calculator.establish_cbl_basis(config)
    baseline = cbl_calculator.calculate_baseline(cbl_basis, config)

    return baseline
