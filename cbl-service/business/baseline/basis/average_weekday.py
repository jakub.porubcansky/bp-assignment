from datetime import date, time
from statistics import mean
import pandas as pd
from business.models import CBLCalculationConfig, TimeInterval
from business.data_access import (
    DataAccessInterface,
    ENERGY_TIMESTAMP,
    ENERGY_METER,
    HOLIDAYS_TIMESTAMP,
    EVENTS_TIMESTAMP_START,
    EVENTS_TIMEZONE
)
from exceptions import ValidationException

INITIAL_WINDOW_SIZE_TO_EXAMINE = 30
SUFFICIENT_WINDOW_LENGTH = 10
SUFFICIENT_BASIS_LENGTH = 5
LOW_USAGE_RATIO_THRESHOLD = 0.25


class AverageWeekdayCBLBasisEstablisher:
    def __init__(self, data_interface: DataAccessInterface) -> None:
        self.data_interface = data_interface
        self.ineligible_days_eliminator = CBLWindowIneligibleDaysEliminator(data_interface)

    def establish(self, config: CBLCalculationConfig) -> dict[date, float]:
        energy_data = self._get_sorted_hourly_energy_data(config)
        peak_hourly_load = self._calculate_peak_hourly_load(energy_data)
        energy_data = self.ineligible_days_eliminator.eliminate(energy_data, config.event_window)
        baseline_window_with_usage = self._establish_window(energy_data, peak_hourly_load, config)
        baseline_basis = self._establish_basis(baseline_window_with_usage)
        return baseline_basis

    def _get_sorted_hourly_energy_data(self, config: CBLCalculationConfig) -> pd.DataFrame:
        energy_data = self._get_energy_data(config)
        energy_data.sort_values(by=ENERGY_TIMESTAMP, ascending=True, inplace=True)

        aggregation_fns = self._get_aggregation_fns()
        resampled = energy_data.resample(on=ENERGY_TIMESTAMP, rule='h').agg(aggregation_fns).reset_index(drop=False)

        resampled_without_missing = resampled.dropna(how='any')

        return resampled_without_missing
    
    def _get_aggregation_fns(self) -> dict[str, callable]:
        return {
            ENERGY_METER: lambda x: sum(x) if not x.empty else None
        }

    def _get_energy_data(self, config: CBLCalculationConfig) -> pd.DataFrame:
        datetime_interval = self._construct_datetime_interval_to_get_energy_data(config.event_window)
        time_interval = self._construct_time_interval_to_get_energy_data(config.event_window)

        energy_data = self.data_interface.get_energy_data(
            building_id=config.building_id,
            datetime_interval=datetime_interval,
        )

        if energy_data.empty:
            raise ValidationException("No energy data available for the specified building and time intervals")

        energy_data[ENERGY_TIMESTAMP] = energy_data[ENERGY_TIMESTAMP].dt.tz_convert(config.timezone)

        energy_data = energy_data[(energy_data[ENERGY_TIMESTAMP].dt.time >= time_interval.start) &
                                  (energy_data[ENERGY_TIMESTAMP].dt.time < time_interval.end)]
        return energy_data[[ENERGY_TIMESTAMP, ENERGY_METER]]

    def _construct_datetime_interval_to_get_energy_data(self, event_window: TimeInterval[pd.Timestamp]) -> TimeInterval[pd.Timestamp]:
        return TimeInterval(
            event_window.start.floor('D') - pd.Timedelta(days=INITIAL_WINDOW_SIZE_TO_EXAMINE),
            event_window.end.floor('D')
        )

    def _construct_time_interval_to_get_energy_data(self, event_window: TimeInterval[pd.Timestamp]) -> TimeInterval[time]:
        return TimeInterval(
            event_window.start.time(),
            event_window.end.time()
        )

    def _calculate_peak_hourly_load(self, energy_data: pd.DataFrame) -> float:
        return energy_data[ENERGY_METER].max()

    def _establish_window(self, energy_data: pd.DataFrame, peak_hourly_load: float, config: CBLCalculationConfig) -> dict[date, float]:
        baseline_window_with_usage = self._establish_window_core(energy_data, peak_hourly_load,
                                                                 config, eliminate_low_usage_days=True)

        if not self._has_window_sufficient_length(len(baseline_window_with_usage)):
            baseline_window_with_usage = self._establish_window_core(energy_data, peak_hourly_load, config,
                                                                     eliminate_low_usage_days=False)

        if not self._has_window_sufficient_length(len(baseline_window_with_usage)):
            raise ValidationException("Not enough data to establish a baseline window")

        return baseline_window_with_usage

    def _establish_window_core(self, energy_data: pd.DataFrame, peak_hourly_load: float,
                               config: CBLCalculationConfig, eliminate_low_usage_days: bool) -> dict[date, float]:
        unique_dates_sorted = sorted(energy_data.timestamp.dt.date.unique().tolist(), reverse=True)
        average_daily_event_period_usage = {}

        for date_to_examine in unique_dates_sorted:
            event_window_to_examine = self._get_event_window_to_examine(date_to_examine, config)
            average_usage_to_examine = self._calculate_average_daily_event_period_usage(energy_data, event_window_to_examine)

            to_eliminate = False
            if eliminate_low_usage_days:
                is_low_usage_day = self._check_if_low_usage_day(average_usage_to_examine, average_daily_event_period_usage, peak_hourly_load)
                to_eliminate = True if is_low_usage_day else False

            if not to_eliminate:
                average_daily_event_period_usage[date_to_examine] = average_usage_to_examine

            current_window_length = len(average_daily_event_period_usage)
            if self._has_window_sufficient_length(current_window_length):
                break

        return average_daily_event_period_usage

    def _get_event_window_to_examine(self, date_to_examine: date, config: CBLCalculationConfig) -> TimeInterval[pd.Timestamp]:
        return TimeInterval(
            start=pd.Timestamp.combine(date_to_examine, config.event_window.start.time()).replace(tzinfo=config.timezone),
            end=pd.Timestamp.combine(date_to_examine, config.event_window.end.time()).replace(tzinfo=config.timezone)
        )

    def _calculate_average_daily_event_period_usage(self, energy_data: pd.DataFrame, event_window: TimeInterval[pd.Timestamp]) -> float:
        event_window_energy_data = energy_data[(energy_data.timestamp.dt.time >= event_window.start.time()) &
                                               (energy_data.timestamp.dt.time < event_window.end.time()) &
                                               (energy_data.timestamp.dt.date == event_window.start.date())]
        return event_window_energy_data[ENERGY_METER].mean()

    def _has_window_sufficient_length(self, window_length: int) -> bool:
        return window_length == SUFFICIENT_WINDOW_LENGTH

    def _check_if_low_usage_day(self, average_usage_to_examine: float, average_daily_event_period_usage: dict[date, float],
                                peak_hourly_load: float) -> bool:
        if average_daily_event_period_usage:
            average_event_period_usage_level = mean(average_daily_event_period_usage.values())
        else:
            average_event_period_usage_level = peak_hourly_load

        return self._is_low_usage(average_usage_to_examine, average_event_period_usage_level)

    def _is_low_usage(self, usage: float, usage_level: float) -> bool:
        return usage < LOW_USAGE_RATIO_THRESHOLD * usage_level

    def _establish_basis(self, baseline_window: dict[date, float]) -> list[date]:
        sorted_usages = sorted(baseline_window.items(), key=lambda x: x[1], reverse=True)
        basis_dates = [date for date, _ in sorted_usages[0:SUFFICIENT_BASIS_LENGTH]]
        return basis_dates


class CBLWindowIneligibleDaysEliminator:
    def __init__(self, data_interface: DataAccessInterface) -> None:
        self.data_interface = data_interface

    def eliminate(self, energy_data: pd.DataFrame, event_window: TimeInterval[pd.Timestamp]) -> pd.DataFrame:
        energy_data_filtered = self._eliminate_weekends(energy_data)
        energy_data_filtered = self._eliminate_holidays(energy_data_filtered)
        energy_data_filtered = self._eliminate_past_events(energy_data_filtered)
        energy_data_filtered = self._eliminate_day_before_the_event(energy_data_filtered, event_window)
        return energy_data_filtered

    def _eliminate_weekends(self, energy_data: pd.DataFrame) -> pd.DataFrame:
        return energy_data[energy_data.timestamp.dt.dayofweek <= 4]

    def _eliminate_holidays(self, energy_data: pd.DataFrame) -> pd.DataFrame:
        date_interval = self._get_min_max_date_interval(energy_data)
        holidays = self._get_holidays_dates(date_interval)
        indices_of_non_holidays = ~energy_data.timestamp.dt.date.isin(holidays)
        return energy_data[indices_of_non_holidays]

    def _get_min_max_date_interval(self, energy_data: pd.DataFrame) -> TimeInterval[date]:
        datetime_interval = self._get_min_max_datetime_interval(energy_data)
        return TimeInterval(datetime_interval.start.date(), datetime_interval.end.date())

    def _get_holidays_dates(self, date_interval: TimeInterval[date]) -> list[date]:
        holidays_data = self.data_interface.get_holidays_data(date_interval)
        return holidays_data[HOLIDAYS_TIMESTAMP].dt.date.unique().tolist()

    def _eliminate_past_events(self, energy_data: pd.DataFrame) -> pd.DataFrame:
        datetime_interval = self._get_min_max_datetime_interval(energy_data)
        event_dates = self._get_events_dates(datetime_interval)
        prior_event_dates = [date - pd.Timedelta(days=1) for date in event_dates]
        all_dates_to_eliminate = event_dates.union(prior_event_dates)
        indices_of_non_event_dates_and_priors = ~energy_data.timestamp.dt.date.isin(all_dates_to_eliminate)
        return energy_data[indices_of_non_event_dates_and_priors]

    def _get_min_max_datetime_interval(self, energy_data: pd.DataFrame) -> TimeInterval[pd.Timestamp]:
        return TimeInterval(energy_data.timestamp.min(), energy_data.timestamp.max())

    def _get_events_dates(self, datetime_interval: TimeInterval[pd.Timestamp]) -> set[date]:
        events_data = self.data_interface.get_events_data(datetime_interval)
        events_dates = self._get_events_dates_from_events_data(events_data)
        return events_dates
    
    def _get_events_dates_from_events_data(self, events_data: pd.DataFrame) -> set[date]:
        events_dates = set()
        for _, row in events_data.iterrows():
            timezone = row[EVENTS_TIMEZONE]
            timestamp_converted = row[EVENTS_TIMESTAMP_START].tz_convert(timezone)
            events_dates.add(timestamp_converted.date())

        return events_dates

    def _eliminate_day_before_the_event(self, energy_data: pd.DataFrame, event_window: TimeInterval[pd.Timestamp]) -> pd.DataFrame:
        yesterday_date = event_window.start.date() - pd.Timedelta(days=1)
        indices_of_non_yesterday_data = energy_data.timestamp.dt.date != yesterday_date
        return energy_data[indices_of_non_yesterday_data]
