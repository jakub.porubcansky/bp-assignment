from datetime import date, time
import pandas as pd
from business.models import CBLCalculationConfig, TimeInterval
from business.data_access import (
    DataAccessInterface,
    ENERGY_TIMESTAMP,
    ENERGY_METER
)
from exceptions import ValidationException

SUFFICIENT_WINDOW_LENGTH = 3
SUFFICIENT_BASIS_LENGTH = 2


class AverageWeekendCBLBasisEstablisher:
    def __init__(self, data_interface: DataAccessInterface) -> None:
        self.data_interface = data_interface

    def establish(self, config: CBLCalculationConfig) -> dict[date, float]:
        energy_data = self._get_sorted_hourly_energy_data(config)
        energy_data = self._eliminate_ineligible_days(energy_data, config.event_window)
        energy_data = self._filter_event_timestamps(energy_data, config.event_window)
        baseline_window_with_usage = self._calculate_average_daily_event_period_usage(energy_data)
        baseline_basis = self._establish_basis(baseline_window_with_usage)
        return baseline_basis

    def _get_sorted_hourly_energy_data(self, config: CBLCalculationConfig) -> pd.DataFrame:
        energy_data = self._get_energy_data(config)

        energy_data.sort_values(by=ENERGY_TIMESTAMP, ascending=True, inplace=True)

        aggregation_fns = self._get_aggregation_fns()
        resampled = energy_data.resample(on=ENERGY_TIMESTAMP, rule='h').agg(aggregation_fns).reset_index(drop=False)

        resampled_without_missing = resampled.dropna(how='any')

        return resampled_without_missing
    
    def _get_aggregation_fns(self) -> dict[str, callable]:
        return {
            ENERGY_METER: lambda x: sum(x) if not x.empty else None
        }

    def _get_energy_data(self, config: CBLCalculationConfig) -> pd.DataFrame:
        datetime_interval = self._construct_datetime_interval_to_get_energy_data(config.event_window)
        time_interval = self._construct_time_interval_to_get_energy_data(config.event_window)

        energy_data = self.data_interface.get_energy_data(
            building_id=config.building_id,
            datetime_interval=datetime_interval,
        )

        if energy_data.empty:
            raise ValidationException("No energy data available for the specified building and time intervals")
       
        energy_data[ENERGY_TIMESTAMP] = energy_data[ENERGY_TIMESTAMP].dt.tz_convert(config.timezone)

        energy_data = energy_data[(energy_data[ENERGY_TIMESTAMP].dt.time >= time_interval.start) &
                                  (energy_data[ENERGY_TIMESTAMP].dt.time < time_interval.end)]
        
        return energy_data[[ENERGY_TIMESTAMP, ENERGY_METER]]

    def _construct_datetime_interval_to_get_energy_data(self, event_window: TimeInterval[pd.Timestamp]) -> TimeInterval[pd.Timestamp]:
        return TimeInterval(
            event_window.start.floor('D') - pd.Timedelta(weeks=SUFFICIENT_WINDOW_LENGTH),
            event_window.end.floor('D')
        )

    def _construct_time_interval_to_get_energy_data(self, event_window: TimeInterval[pd.Timestamp]) -> TimeInterval[time]:
        return TimeInterval(
            event_window.start.time(),
            event_window.end.time()
        )

    def _eliminate_ineligible_days(self, energy_data: pd.DataFrame, event_window: TimeInterval[pd.Timestamp]) -> pd.DataFrame:
        event_day_of_week = event_window.start.dayofweek
        return energy_data[energy_data.timestamp.dt.dayofweek == event_day_of_week]

    def _filter_event_timestamps(self, energy_data: pd.DataFrame, event_window: TimeInterval[pd.Timestamp]) -> pd.DataFrame:
        return energy_data[(energy_data.timestamp.dt.time >= event_window.start.time()) &
                           (energy_data.timestamp.dt.time < event_window.end.time())]

    def _calculate_average_daily_event_period_usage(self, energy_data: pd.DataFrame) -> dict[date, float]:
        aggregated = energy_data.groupby(energy_data.timestamp.dt.date).mean()
        aggregated.drop(columns=ENERGY_TIMESTAMP, inplace=True)
        return aggregated.to_dict()[ENERGY_METER]

    def _establish_basis(self, baseline_window_with_usage: dict[date, float]) -> list[date]:
        sorted_usages = sorted(baseline_window_with_usage.items(), key=lambda x: x[1], reverse=True)
        basis_dates = [date for date, _ in sorted_usages[0:SUFFICIENT_BASIS_LENGTH]]
        return basis_dates
