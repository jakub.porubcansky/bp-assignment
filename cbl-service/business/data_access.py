from typing import Protocol, Optional
from datetime import date, time
import pandas as pd
from business.models import TimeInterval

ENERGY_TIMESTAMP = 'timestamp'
ENERGY_TIMEZONE = 'timezone'
ENERGY_METER = 'energy_kwh'
EVENTS_TIMESTAMP_START = 'event_start'
EVENTS_TIMESTAMP_END = 'event_end'
EVENTS_TIMEZONE = 'timezone'
HOLIDAYS_TIMESTAMP = 'timestamp'


class DataAccessInterface(Protocol):
    def get_energy_data(self, building_id: str, datetime_interval: TimeInterval[pd.Timestamp]) -> pd.DataFrame:
        ...

    def get_events_data(self, datetime_interval: TimeInterval[pd.Timestamp]) -> pd.DataFrame:
        ...

    def get_holidays_data(self, date_interval: TimeInterval[date]) -> pd.DataFrame:
        ...
