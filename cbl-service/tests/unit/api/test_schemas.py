import pytest
from datetime import timedelta
from api.schemas import Granularity, BaselineCalculationInput
from exceptions import ValidationException


class TestGranularity:
    def test_to_timedelta(self):
        result1 = Granularity.MINUTE_5.to_timedelta()
        result2 = Granularity.MINUTE_15.to_timedelta()
        result3 = Granularity.HOUR.to_timedelta()

        assert result1 == timedelta(minutes=5)
        assert result2 == timedelta(minutes=15)
        assert result3 == timedelta(hours=1)


class TestBaselineCalculationInput:
    def _model_validate(self, obj):
        BaselineCalculationInput.model_validate(obj)

    def _model_construct(self, obj):
        BaselineCalculationInput.model_construct(**obj)

    def _model_validate_and_construct(self, obj):
        self._model_validate(obj)
        self._model_construct(obj)

    def test_valid_1(self):
        input = {
            "building_id": "test",
            "timezone": "UTC",
            "start": "2024-01-01T00:00:00Z",
            "end": "2024-01-01T01:00:00Z",
            "granularity": "5min"
        }

        self._model_validate_and_construct(input)

    def test_valid_2(self):
        input = {
            "building_id": "test",
            "timezone": "Europe/Budapest",
            "start": "2024-01-01T00:00:00+00:00",
            "end": "2024-01-01T01:00:00+00:00",
            "granularity": "15min"
        }

        self._model_validate_and_construct(input)

    def test_valid_3(self):
        input = {
            "building_id": "test",
            "timezone": "US/Central",
            "start": "2024-01-01T00:00:00+00:00",
            "end": "2024-01-01T01:00:00+00:00",
            "granularity": "1hour"
        }

        self._model_validate_and_construct(input)

    def test_invalid_timezone(self):
        input = {
            "building_id": "test",
            "timezone": "invalid",
            "start": "2024-01-01T00:00:00+00:00",
            "end": "2024-01-01T00:00:00+00:00",
            "granularity": "5min"
        }

        with pytest.raises(ValidationException):
            self._model_validate_and_construct(input)

    def test_invalid_event_window_start_timezone(self):
        input = {
            "building_id": "test",
            "timezone": "UTC",
            "start": "2024-01-01T00:00:00+01:00",
            "end": "2024-01-01T00:00:00+00:00",
            "granularity": "5min"
        }

        with pytest.raises(ValidationException):
            self._model_validate_and_construct(input)

    def test_invalid_event_window_end_timezone(self):
        input = {
            "building_id": "test",
            "timezone": "UTC",
            "start": "2024-01-01T00:00:00+00:00",
            "end": "2024-01-01T00:00:00+01:00",
            "granularity": "5min"
        }

        with pytest.raises(ValidationException):
            self._model_validate_and_construct(input)

    def test_invalid_event_window_interval(self):
        input = {
            "building_id": "test",
            "timezone": "UTC",
            "start": "2024-01-01T01:00:00+00:00",
            "end": "2024-01-01T00:00:00+00:00",
            "granularity": "5min"
        }

        with pytest.raises(ValidationException):
            self._model_validate_and_construct(input)

    def test_invalid_granularity_1(self):
        input = {
            "building_id": "test",
            "timezone": "UTC",
            "start": "2024-01-01T00:00:00+00:00",
            "end": "2024-01-01T00:00:00+00:00",
            "granularity": "invalid"
        }

        with pytest.raises(ValueError):
            self._model_validate_and_construct(input)

    def test_invalid_granularity_2(self):
        input = {
            "building_id": "test",
            "timezone": "UTC",
            "start": "2024-01-01T00:00:00+00:00",
            "end": "2024-01-01T00:00:00+00:00",
            "granularity": "4min"
        }

        with pytest.raises(ValueError):
            self._model_validate_and_construct(input)
