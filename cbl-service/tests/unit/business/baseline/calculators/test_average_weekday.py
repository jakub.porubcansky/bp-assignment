# from io import StringIO
# from datetime import time
# import pandas as pd
# from business.baseline.calculators.average_weekday import CBLWindowEstablisher
# from business.models import CBLCalculationConfig, TimeInterval


# class TestDataAccessor:
#     def get_energy_data(self, building_id: str, interval: TimeInterval[pd.Timestamp]) -> pd.DataFrame:
#         csv_data = """timestamp,energy_kwh\n
#         2018-01-01 00:00:00+04:00,10.0\n
#         2018-01-01 00:05:00+04:00,27.0\n
#         2018-01-01 00:10:00+04:00,40.0\n
#         2018-01-01 00:50:00+04:00,22.0\n
#         2018-01-01 00:55:00+04:00,11.0\n
#         2018-01-01 01:00:00+04:00,50.0\n
#         2018-01-01 01:05:00+04:00,56.0\n
#         2018-01-01 01:10:00+04:00,48.0\n
#         2018-01-01 01:15:00+04:00,16.0\n
#         2018-01-01 02:00:00+04:00,25.0\n
#         """

#         return pd.read_csv(StringIO(csv_data), parse_dates=['timestamp'])


# class TestCBLWindowEstablisher:
#     def test_calculate_peak_hourly_load(self):
#         calculator = CBLWindowEstablisher(TestDataAccessor())

#         config = CBLCalculationConfig(
#             building_id='test',
#             event_window=TimeInterval(
#                 pd.Timestamp('2018-01-01 00:00:00+04:00'),
#                 pd.Timestamp('2018-01-01 02:00:00+04:00')
#             ),
#             timezone=pytz.timezone('UTC'),
#             granularity=pd.Timedelta(minutes=5)
#         )

#         result = calculator._calculate_peak_hourly_load(config)

#         assert result == 170.0
