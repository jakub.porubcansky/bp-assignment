import pytest
import pytz
import pandas as pd
from business.models import CBLCalculationConfig, TimeInterval
from exceptions import ValidationException

class TestBaselineCalculationConfig:
    def test_is_weekend_baseline(self):
        config = CBLCalculationConfig(
            building_id="test",
            event_window=TimeInterval(
                start=pd.Timestamp("2024-01-01T04:00:00+00:00"),
                end=pd.Timestamp("2024-01-01T08:00:00+00:00")
            ),
            timezone=pytz.timezone('UTC'),
            granularity=pd.Timedelta("5min")
        )

        assert config.is_weekend_baseline is False

        config.event_window = TimeInterval(
            start=pd.Timestamp("2024-01-05T04:00:00+00:00"),
            end=pd.Timestamp("2024-01-05T08:00:00+00:00")
        )

        assert config.is_weekend_baseline is False

        config.event_window = TimeInterval(
            start=pd.Timestamp("2024-01-06T04:00:00+00:00"),
            end=pd.Timestamp("2024-01-06T08:00:00+00:00")
        )

        assert config.is_weekend_baseline is True

        config.event_window = TimeInterval(
            start=pd.Timestamp("2024-01-07T04:00:00+00:00"),
            end=pd.Timestamp("2024-01-07T08:00:00+00:00")
        )

        assert config.is_weekend_baseline is True

    def test_invalid_event_window_interval_spanning_multiple_days(self):
        with pytest.raises(ValidationException):
            CBLCalculationConfig(
                building_id="test",
                event_window=TimeInterval(
                    start=pd.Timestamp("2024-01-01T00:00:00+00:00"),
                    end=pd.Timestamp("2024-01-02T00:00:00+00:00")
                ),
                timezone=pytz.timezone('UTC'),
                granularity=pd.Timedelta("5min")
            )

    def test_invalid_event_window_interval_spanning_multiple_days_2(self):
        with pytest.raises(ValidationException):
            CBLCalculationConfig(
                building_id="test",
                event_window=TimeInterval(
                    start=pd.Timestamp("2024-01-01T00:00:00+05:00"),
                    end=pd.Timestamp("2024-01-02T00:00:00+05:00")
                ),
                timezone=pytz.timezone('UTC'),
                granularity=pd.Timedelta("5min")
            )
