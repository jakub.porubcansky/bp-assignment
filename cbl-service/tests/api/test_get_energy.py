import io
import pandas as pd


def validate_response(response, n_expected_rows: int, left_timestamp_bound: pd.Timestamp,
                      right_timestamp_bound: pd.Timestamp, granularity: pd.Timedelta):
    dataframe = pd.read_csv(io.StringIO(response.text))

    assert response.status_code == 200
    assert dataframe.shape[0] == n_expected_rows
    assert 'timestamp' in dataframe.columns
    assert 'energy_kwh' in dataframe.columns
    assert 'energy_kw' in dataframe.columns

    dataframe['timestamp'] = pd.to_datetime(dataframe['timestamp'])

    if n_expected_rows > 1:
        assert dataframe.timestamp.diff().median() == granularity

    assert all(dataframe.timestamp >= left_timestamp_bound)
    assert all(dataframe.timestamp < right_timestamp_bound)


def test_valid(client):
    query_params = {
        "building_id": "Building A",
        "start": "2018-02-01T10:00:00+00:00",
        "end": "2018-02-01T13:00:00+00:00",
        "granularity": "15min"
    }

    response = client.get('/energy', params=query_params)

    validate_response(
        response=response,
        n_expected_rows=12,
        left_timestamp_bound=pd.Timestamp('2018-02-01 10:00:00+00:00'),
        right_timestamp_bound=pd.Timestamp('2018-02-01 13:00:00+00:00'),
        granularity=pd.Timedelta('15min')
    )
