import io
import pandas as pd


def validate_response(response, n_expected_rows: int, left_timestamp_bound: pd.Timestamp,
                      right_timestamp_bound: pd.Timestamp, granularity: pd.Timedelta):
    dataframe = pd.read_csv(io.StringIO(response.text))

    assert response.status_code == 200
    assert dataframe.shape[0] == n_expected_rows
    assert 'timestamp' in dataframe.columns
    assert 'baseline_kw' in dataframe.columns

    dataframe['timestamp'] = pd.to_datetime(dataframe['timestamp'])

    if n_expected_rows > 1:
        assert dataframe.timestamp.diff().median() == granularity

    assert all(dataframe.timestamp >= left_timestamp_bound)
    assert all(dataframe.timestamp < right_timestamp_bound)


def test_valid_weekday(client):
    payload = {
        "building_id": "Building A",
        "timezone": "America/New_York",
        "start": "2018-02-01 10:00:00+00:00",
        "end": "2018-02-01 15:00:00+00:00",
        "granularity": "5min"
    }

    response = client.post('/calculate-baseline', json=payload)

    validate_response(
        response=response,
        n_expected_rows=60,
        left_timestamp_bound=pd.Timestamp('2018-02-01 10:00:00+00:00'),
        right_timestamp_bound=pd.Timestamp('2018-02-01 15:00:00+00:00'),
        granularity=pd.Timedelta('5min')
    )


def test_valid_weekday_2(client):
    payload = {
        "building_id": "Building B",
        "timezone": "US/Central",
        "start": "2018-02-07 15:00:00+00:00",
        "end": "2018-02-07 15:15:00+00:00",
        "granularity": "15min"
    }

    response = client.post('/calculate-baseline', json=payload)

    validate_response(
        response=response,
        n_expected_rows=1,
        left_timestamp_bound=pd.Timestamp('2018-02-07 15:00:00+00:00'),
        right_timestamp_bound=pd.Timestamp('2018-02-07 15:15:00+00:00'),
        granularity=pd.Timedelta('15min')
    )


def test_valid_weekday_3(client):
    payload = {
        "building_id": "Building C",
        "timezone": "America/New_York",
        "start": "2018-02-12 13:00:00+00:00",
        "end": "2018-02-12 15:30:00+00:00",
        "granularity": "1hour"
    }

    response = client.post('/calculate-baseline', json=payload)

    validate_response(
        response=response,
        n_expected_rows=3,
        left_timestamp_bound=pd.Timestamp('2018-02-12 13:00:00+00:00'),
        right_timestamp_bound=pd.Timestamp('2018-02-12 15:30:00+00:00'),
        granularity=pd.Timedelta('1hour')
    )


def test_valid_weekday_no_granularity(client):
    payload = {
        "building_id": "Building A",
        "timezone": "America/New_York",
        "start": "2018-02-01 10:00:00+00:00",
        "end": "2018-02-01 15:00:00+00:00",
    }

    response = client.post('/calculate-baseline', json=payload)

    validate_response(
        response=response,
        n_expected_rows=60,
        left_timestamp_bound=pd.Timestamp('2018-02-01 10:00:00+00:00'),
        right_timestamp_bound=pd.Timestamp('2018-02-01 15:00:00+00:00'),
        granularity=pd.Timedelta('5min')
    )


def test_valid_weekend(client):
    payload = {
        "building_id": "Building A",
        "timezone": "America/New_York",
        "start": "2018-02-03 10:00:00+00:00",
        "end": "2018-02-03 15:00:00+00:00",
        "granularity": "5min"
    }

    response = client.post('/calculate-baseline', json=payload)

    validate_response(
        response=response,
        n_expected_rows=60,
        left_timestamp_bound=pd.Timestamp('2018-02-03 10:00:00+00:00'),
        right_timestamp_bound=pd.Timestamp('2018-02-03 15:00:00+00:00'),
        granularity=pd.Timedelta('5min')
    )


def test_valid_weekend_2(client):
    payload = {
        "building_id": "Building A",
        "timezone": "America/New_York",
        "start": "2018-02-04 11:00:00+00:00",
        "end": "2018-02-04 12:00:00+00:00",
        "granularity": "1hour"
    }

    response = client.post('/calculate-baseline', json=payload)

    validate_response(
        response=response,
        n_expected_rows=1,
        left_timestamp_bound=pd.Timestamp('2018-02-04 11:00:00+00:00'),
        right_timestamp_bound=pd.Timestamp('2018-02-04 12:00:00+00:00'),
        granularity=pd.Timedelta('1hour')
    )


def test_invalid_weekday_no_data(client):
    payload = {
        "building_id": "Building C",
        "timezone": "America/New_York",
        "start": "2017-02-09 13:00:00+00:00",
        "end": "2017-02-09 15:30:00+00:00",
        "granularity": "1hour"
    }

    response = client.post('/calculate-baseline', json=payload)

    assert response.status_code == 400


def test_invalid_weekend_no_data(client):
    payload = {
        "building_id": "Building C",
        "timezone": "America/New_York",
        "start": "2017-02-10 13:00:00+00:00",
        "end": "2017-02-10 15:30:00+00:00",
        "granularity": "1hour"
    }

    response = client.post('/calculate-baseline', json=payload)

    assert response.status_code == 400


def test_invalid_config(client):
    payload = {
        "building_id": "test",
    }

    response = client.post('/calculate-baseline', json=payload)

    assert response.status_code == 422
