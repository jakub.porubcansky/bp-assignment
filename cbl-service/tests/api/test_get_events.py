import io
import pandas as pd


def validate_response(response, n_expected_rows: int, left_timestamp_bound: pd.Timestamp,
                      right_timestamp_bound: pd.Timestamp):
    dataframe = pd.read_csv(io.StringIO(response.text))

    assert response.status_code == 200
    assert dataframe.shape[0] == n_expected_rows
    assert 'event_start' in dataframe.columns
    assert 'event_end' in dataframe.columns

    dataframe['event_start'] = pd.to_datetime(dataframe['event_start'])
    dataframe['event_end'] = pd.to_datetime(dataframe['event_end'])

    assert all(dataframe.event_end >= left_timestamp_bound)
    assert all(dataframe.event_start < right_timestamp_bound)


def test_valid(client):
    query_params = {
        "start": "2018-01-01T00:00:00+00:00",
        "end": "2018-12-31T00:00:00+00:00",
    }

    response = client.get('/events', params=query_params)

    validate_response(
        response=response,
        n_expected_rows=30,
        left_timestamp_bound=pd.Timestamp('2018-01-01T00:00:00+00:00'),
        right_timestamp_bound=pd.Timestamp('2018-12-31T00:00:00+00:00'),
    )
