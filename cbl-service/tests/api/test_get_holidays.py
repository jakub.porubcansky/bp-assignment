import io
import pandas as pd


def validate_response(response, n_expected_rows: int, left_timestamp_bound: pd.Timestamp,
                      right_timestamp_bound: pd.Timestamp):
    dataframe = pd.read_csv(io.StringIO(response.text))

    assert response.status_code == 200
    assert dataframe.shape[0] == n_expected_rows
    assert 'timestamp' in dataframe.columns
    assert 'holiday_name' in dataframe.columns

    dataframe['timestamp'] = pd.to_datetime(dataframe['timestamp'])

    assert all(dataframe.timestamp >= left_timestamp_bound)
    assert all(dataframe.timestamp < right_timestamp_bound)


def test_valid(client):
    query_params = {
        "start": "2018-01-01",
        "end": "2018-12-31",
    }

    response = client.get('/holidays', params=query_params)

    validate_response(
        response=response,
        n_expected_rows=12,
        left_timestamp_bound=pd.Timestamp('2018-01-01'),
        right_timestamp_bound=pd.Timestamp('2018-12-31'),
    )
