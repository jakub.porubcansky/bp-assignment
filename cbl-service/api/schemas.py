from enum import Enum
from datetime import timedelta
from pydantic import BaseModel, AwareDatetime, validator, model_validator
from typing_extensions import Self
import pytz
import pandas as pd
from business.models import CBLCalculationConfig, TimeInterval
from exceptions import ValidationException


class Granularity(Enum):
    MINUTE_5 = "5min"
    MINUTE_15 = "15min"
    HOUR = "1hour"

    def to_timedelta(self) -> timedelta:
        unit, value = self._parse_value()
        return timedelta(**{unit: value})

    def _parse_value(self) -> tuple[int, str]:
        if self.value.endswith('sec'):
            return 'seconds', int(self.value.rstrip('sec'))
        elif self.value.endswith('min'):
            return 'minutes', int(self.value.rstrip('min'))
        elif self.value.endswith('hour'):
            return 'hours', int(self.value.rstrip('hour'))
        elif self.value.endswith('day'):
            return 'days', int(self.value.rstrip('day'))
        else:
            raise ValueError('Invalid granularity')


class BaselineCalculationInput(BaseModel):
    building_id: str
    timezone: str
    start: AwareDatetime
    end: AwareDatetime
    granularity: Granularity = Granularity.MINUTE_5

    @validator('start', 'end', pre=False)
    @classmethod
    def validate_datetime(cls, value):
        if not is_in_utc(value):
            raise ValidationException('start and end must be in UTC timezone.')

        return value

    @validator('timezone', pre=False)
    @classmethod
    def validate_timezone(cls, value):
        try:
            pytz.timezone(value)
        except pytz.UnknownTimeZoneError:
            raise ValidationException("Invalid timezone")

        return value

    @model_validator(mode='after')
    def validate_event_window(self) -> Self:
        if self.start >= self.end:
            raise ValidationException("Baseline window start timestamp must be lower than baseline window end timestamp")

        return self

    def to_config(self) -> CBLCalculationConfig:
        timezone = pytz.timezone(self.timezone)
        window_start_in_timezone = self.start.astimezone(timezone)
        window_end_in_timezone = self.end.astimezone(timezone)

        return CBLCalculationConfig(
            building_id=self.building_id,
            event_window=TimeInterval(
                start=pd.Timestamp(window_start_in_timezone),
                end=pd.Timestamp(window_end_in_timezone)
            ),
            timezone=timezone,
            granularity=self.granularity.to_timedelta(),
        )


def is_in_utc(timestamp) -> bool:
    return timestamp.utcoffset() == timedelta(0)
