from datetime import date
import sqlite3
from fastapi import APIRouter, Response, Depends
from pydantic import AwareDatetime
import pandas as pd
from api.schemas import BaselineCalculationInput, Granularity, is_in_utc
from business.baseline.calculation import calculate_baseline
from business.baseline.factory import cbl_calculator_factory
from business.models import TimeInterval
from business.data_query.energy import get_energy
from business.data_query.events import get_events
from business.data_query.holidays import get_holidays
from db.access import DBAccessor
from exceptions import ValidationException

router = APIRouter()

DB_FILE_NAME = 'cbl.db'

def get_db_connection():
    conn = sqlite3.connect(DB_FILE_NAME)
    try:
        yield conn
    finally:
        conn.close()

@router.get("/health-check")
def health_check_endpoint():
    return {"status": "ok"}

@router.post("/calculate-baseline")
def calculate_baseline_endpoint(input: BaselineCalculationInput, db_conn = Depends(get_db_connection)):
    db_accessor = DBAccessor(db_conn)
    config = input.to_config()

    cbl_calculator = cbl_calculator_factory(config, db_accessor)

    result = calculate_baseline(cbl_calculator, config)

    return dataframe_to_csv_response(result)


@router.get("/holidays")
def get_holidays_endpoint(start: date, end: date, db_conn = Depends(get_db_connection)):
    db_accessor = DBAccessor(db_conn)

    result = get_holidays(db_accessor, TimeInterval(start, end))

    return dataframe_to_csv_response(result)


@router.get("/events")
def get_events_endpoint(start: AwareDatetime, end: AwareDatetime, db_conn = Depends(get_db_connection)):
    db_accessor = DBAccessor(db_conn)

    if not is_in_utc(start) or not is_in_utc(end):
        raise ValidationException('start and end query parameters must be in UTC timezone.')

    datetime_interval = TimeInterval(pd.Timestamp(start), pd.Timestamp(end))
    
    result = get_events(db_accessor, datetime_interval)

    return dataframe_to_csv_response(result)


@router.get("/energy")
def get_energy_endpoint(building_id: str, start: AwareDatetime, end: AwareDatetime,
                        granularity: Granularity = Granularity.MINUTE_5,
                        db_conn = Depends(get_db_connection)):
    db_accessor = DBAccessor(db_conn)

    if not is_in_utc(start) or not is_in_utc(end):
        raise ValidationException('start and end query parameters must be in UTC timezone.')
    
    datetime_interval = TimeInterval(pd.Timestamp(start), pd.Timestamp(end))

    result = get_energy(db_accessor, building_id, datetime_interval,
                        granularity.to_timedelta())

    return dataframe_to_csv_response(result)


def dataframe_to_csv_response(dataframe: pd.DataFrame) -> Response:
    return Response(content=dataframe.to_csv(index=False), media_type="text/csv")
