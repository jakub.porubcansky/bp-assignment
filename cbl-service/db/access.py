import sqlite3
from datetime import date
import pandas as pd
from business.models import TimeInterval

ENERGY_DATA_TABLE = "energy"
EVENTS_DATA_TABLE = "events"
HOLIDAYS_DATA_TABLE = "holidays"

class DBAccessor:
    def __init__(self, db_conn: sqlite3.Connection) -> None:
        self.db_conn = db_conn
        
    def get_energy_data(self, building_id: str, datetime_interval: TimeInterval[pd.Timestamp]) -> pd.DataFrame:
        sql_query = f'SELECT * FROM {ENERGY_DATA_TABLE} WHERE building_id = ? AND timestamp >= ? AND timestamp < ?'
        
        params = (building_id, 
                  str(datetime_interval.start.tz_convert('UTC')), 
                  str(datetime_interval.end.tz_convert('UTC')))

        dataframe = pd.read_sql_query(sql_query, self.db_conn, params=params, parse_dates=['timestamp'])

        return dataframe

    def get_events_data(self, datetime_interval: TimeInterval[pd.Timestamp]) -> pd.DataFrame:
        sql_query = f'SELECT * FROM {EVENTS_DATA_TABLE} WHERE event_end >= ? AND event_start < ?'
        
        params = (str(datetime_interval.start.tz_convert('UTC')), 
                  str(datetime_interval.end.tz_convert('UTC')))

        dataframe = pd.read_sql_query(sql_query, self.db_conn, params=params, 
                                      parse_dates=['event_start', 'event_end'])

        return dataframe

    def get_holidays_data(self, date_interval: TimeInterval[date]) -> pd.DataFrame:
        sql_query = f'SELECT * FROM {HOLIDAYS_DATA_TABLE} WHERE timestamp >= ? AND timestamp <= ?'
        
        params = (str(date_interval.start), str(date_interval.end))

        dataframe = pd.read_sql_query(sql_query, self.db_conn, params=params, parse_dates=['timestamp'])

        return dataframe