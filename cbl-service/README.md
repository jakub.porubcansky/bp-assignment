# Customer Baseline Load API service

## Requirements

- Python >= 3.11
- Poetry package manager

## Running without Docker

Install dependencies (before first run):

1. Create new virtual environment: `poetry shell`
2. Install Python dependencies: `poetry install`
3. Leave virtual environment: `exit`

Run service:

1. Activate virtual environment: `poetry shell`
2. Start service: `uvicorn main:app --host 0.0.0.0 --port 8000`
3. Stop service: `Ctrl+C`
4. Leave virtual environment: `exit`

## Running with Docker Compose

1. Start service: `docker-compose up`
2. Stop service: `docker-compose down`

## Docs

To access the Swagger documentation navigate to `http://localhost:8000/docs`. The OpenAPI JSON specification can be found on `http://localhost:8000/openapi.json`.